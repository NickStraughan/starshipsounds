const path               = require('path');
const webpack            = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin  = require('html-webpack-plugin');
const VueLoaderPlugin    = require('vue-loader/lib/plugin');

module.exports = {
  mode: 'development',
  entry: [
    './src/main.js'
  ],
  output: {
    path: path.resolve(__dirname, '../dist/'),
    //publicPath: '/assets/',
    filename: 'main.js'
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: 'vue-loader'
      },
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['babel-preset-env']
          }
        }
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader', 'resolve-url-loader']
      },
      {
        test: /\.scss$/,
        use: [
          "vue-style-loader",     // creates style nodes from JS strings
          "css-loader",           // translates CSS into CommonJS
          'resolve-url-loader',   // Fixes url() issues?
          "sass-loader?sourceMap" // compiles Sass to CSS
        ]
      },
      {
        test: /\.(ttf|eot|jpg|woff|woff2|mp3|gltf|glb)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader'
      },
      {
        test: /\.svg(\?.*)?$/,
        // loader: 'svg-transform-loader',
        use: [
          'svg-inline-loader',
          'svg-transform-loader',

        ]
      }
    ],
  },
  devtool: 'source-map',
  devServer: {
    hot: true,
    port: 9000,
    inline: true,
    contentBase: path.join(__dirname, '../src/'),
    historyApiFallback: true,
    watchOptions: {
      poll: true
    }
  },
  plugins: [
    new CleanWebpackPlugin(['dist']),
    new HtmlWebpackPlugin({
      title: 'Hot Module Replacement',
      template: "./src/index.html"
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new VueLoaderPlugin(),
  ],
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      //'@': path.resolve(__dirname, "src"), // Not working :|
    }
  },
};