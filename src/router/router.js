import Vue       from 'vue';
import VueRouter from 'vue-router';

import Splash        from '../components/Splash';
import Quarters      from '../components/Quarters.vue';
import NotFound      from '../components/NotFound.vue';
import DamagedHelmet from '../components/DamagedHelmet.vue';


export default new VueRouter({
  mode: 'history',
  // Use this if the application is installed in a sub-folder of the web root. (ex: /var/www/html/sss)
  // base: '/sss/',
  routes: [
    {
      path: '*',
      component: NotFound
    },
    {
      path: '/',
      title: 'Home',
      component: Splash
    },
    {
      path: '/quarters',
      title: 'Quarters',
      component: Quarters
    },
    {
      path: '/helmet',
      title: 'Helmet (Three.js example)',
      component: DamagedHelmet
    }
  ],
});