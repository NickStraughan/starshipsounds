import Vue from 'vue';
import VueRouter from 'vue-router';

// Router & Routes
import router from './router/router.js';

// Global Stylesheet
import './styles/style.scss';

// Parent component
import App from './components/App.vue';

// Global Plugins
// import Vuebar from 'vuebar';
// Vue.use(Vuebar);
Vue.use(VueRouter);


// Create and mount the root instance; Inject router
const vm = new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
});
