# :alien: Starship Sounds

This project is a vessel for learning more about Vue.js, Webpack, WebGL/Three.js, the HTML Audio API, and who knows what else.

The webapp-in-progress is a dynamic white noise generator and live screen-saver which is currently very Trek-centric. Eventually the name will change and a more diverse set of themed environments will be available, with fun easter-egg type events for each scene, and support for alarm clocks. The goal is to provide a more dynamic and useful version of what can be seen [here in generic Sci-Fi fashion](https://www.youtube.com/watch?v=O7OWVgr67DM) or [here with a Harry Potter theme](https://www.youtube.com/watch?v=yXq2ziVkK38), combined with a custom white-noise settings panel similar to [A Soft Murmur](https://asoftmurmur.com/).


## Setup / Build

The app is built using [Vue.js](https://vuejs.org/) and [Webpack](https://webpack.js.org/) so to get started all you (should) have to do is:
``` bash
# install dependencies
> npm install

# serve with hot reload at localhost:9000
> npm run dev

# build for production [not configured currently]
> npm run build
```
